# Maemo Leste User Handbook

## Name
Maemo Leste User Handbook

## Description
This is the User Handbook (manual) of Maemo Leste, an operating system for mobile phones, based on Devuan.

## Compile
The Maemo Leste User Handbook consists of reStructuredText files, which can be compiled into HTML, LaTeX, ePUB, and more.
The way it's developed, and the easiest way to render it is by using [Sphinx](https://www.sphinx-doc.org/).

## Support
For support, questions or suggestions, contact us on #maemo-leste on [irc.libera.chat](ircs://irc.libera.chat/#maemo-leste).

## License
The Maemo Leste User Handbook is published under the [GNU FDL 1.3](https://www.gnu.org/licenses/fdl-1.3.en.html) license.

## Project status
The project is in an early development stage. Expect to see frequent and perhaps radical changes.
